﻿namespace BlogCoreAPI.DTOs.Contracts
{
    public interface IHasAuthor
    {
        public int Author { get; }
    }
}
