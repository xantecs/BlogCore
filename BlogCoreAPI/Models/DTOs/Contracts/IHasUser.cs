﻿namespace BlogCoreAPI.DTOs.Contracts
{
    public interface IHasUser
    {
        public int User { get; }
    }
}
